<?php

function timelinejs_configuration(){
	$form = array();
	
	$form['bookmark'] = array(
		'#type' => 'fieldset', 
		'#title' => t('Bookmark content'), 
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE
	);
	$form['bookmark']['timelinejs_bookmark_headline'] = array(
		'#type' => 'textfield', 
		'#title' => t('Headline'),
		'#default_value' => variable_get('timelinejs_bookmark_headline','The Main Timeline Headline Goes here'),
	);
	$form['bookmark']['timelinejs_bookmark_type'] = array(
		'#type' => 'textfield', 
		'#title' => t('Type'),
		'#descripion' => t('Only default type supported by now'),
		'#access' => FALSE,
		'#default_value' => variable_get('timelinejs_bookmark_type','default'),
	);
	$form['bookmark']['timelinejs_bookmark_startdate'] = array(
		'#type' => 'textfield', 
		'#title' => t('Start Date'),
		'#default_value' => variable_get('timelinejs_bookmark_startdate','2000'),
	);
	$form['bookmark']['timelinejs_bookmark_text'] = array(
		'#type' => 'textarea', 
		'#title' => t('Text'),
		'#default_value' => variable_get('timelinejs_bookmark_text','Intro body text goes here, some HTML is ok'),
	);
	$form['bookmark']['timelinejs_bookmark_asset'] = array(
		'#type' => 'fieldset', 
		'#title' => t('Bookmark asset content'), 
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE
	);
	$form['bookmark']['bookmark_asset']['timelinejs_bookmark_asset_credit'] = array(
		'#type' => 'textfield', 
		'#title' => t('Credits'),
		'#default_value' => variable_get('timelinejs_bookmark_asset_credit',''),
	);
	$form['bookmark']['bookmark_asset']['timelinejs_bookmark_asset_caption'] = array(
		'#type' => 'textfield', 
		'#title' => t('Caption'),
		'#default_value' => variable_get('timelinejs_bookmark_asset_caption',''),
	);

	$form['configuration'] = array(
		'#type' => 'fieldset', 
		'#title' => t('Configuration Settings'), 
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE
	);
	$form['configuration']['timelinejs_configuration_width'] = array(
		'#type' => 'textfield', 
		'#title' => t('Width'),
		'#required' => TRUE,
		'#descripion' => t('The width of the timeline in percentage (100%) or in pixels (100)'),
		'#default_value' => variable_get('timelinejs_configuration_width','100%'),
	);
	$form['configuration']['timelinejs_configuration_height'] = array(
		'#type' => 'textfield', 
		'#title' => t('Height'),
		'#required' => TRUE,
		'#descripion' => t('The height of the timeline in percentage (100%) or in pixels (100)'),
		'#default_value' => variable_get('timelinejs_configuration_height','100%'),
	);
	$form['configuration']['timelinejs_configuration_startend'] = array(
		'#type' => 'checkbox', 
		'#title' => t('Start at End'),
		'#return_value' => TRUE, 
      	'#default_value' => FALSE, 
		'#default_value' => variable_get('timelinejs_configuration_startend',FALSE),
	);
	$form['configuration']['timelinejs_configuration_zoom'] = array(
		'#type' => 'textfield', 
		'#title' => t('Zoom'),
		'#default_value' => variable_get('timelinejs_configuration_zoom',3),
	);
	$form['configuration']['timelinejs_configuration_lang'] = array(
		'#type' => 'textfield', 
		'#title' => t('Language'),
		'#descripion' => t('More info at timelinejs/README.markdown'),
		'#default_value' => variable_get('timelinejs_configuration_lang','en'),
	);
	$path = drupal_get_path('module', 'timelinejs');
	$path .= '/css/themes/custom';
	$mask = ".css";
	$css_files = file_scan_directory($path, $mask, array('.', '..', 'CVS'), FALSE, 'filename');

	$options = array('default' => t('Timeline Default'), 'dark' => t('Dark'));
	if(!empty($css_files)){
		foreach($css_files as $value){
			$options[$value->name] = ucfirst($value->name);
		}
	}
	
	$form['configuration']['timelinejs_configuration_theme'] = array(
		'#type' => 'select', 
		'#title' => t('Theme'),
		'#default_value' => variable_get('timelinejs_configuration_theme','default'),
		'#options' => $options
	);
	
  	return system_settings_form($form);
}

function timelinejs_form_alter(&$form, &$form_state, $form_id){
	if($form_id == 'timelinejs_configuration'){

//pispa($form_state,true);

		$file = !isset($form_state['post']['form_id']) ? variable_get('timelinejs_bookmark_asset_media','') : '' ;

		if(isset($file->filepath)){
			$imagem = theme('image',$file->filepath,'timelinejs bookmark image','timelinejs', array('width'=>90,'height'=>90),false);
		} else {
			$imagem = '';
		}

		$form['bookmark']['bookmark_asset']['timelinejs_bookmark_asset_media2'] = array(
			'#type' => 'file',
			'#prefix' => '<div style="padding:5px;margin:0 10px 0 0; float:left;">'.$imagem.'</div>',  
			'#title' => t('Media'),
			'#sufix' => '<div style="clear:both;"></div>',
		);
		$form['bookmark']['bookmark_asset']['timelinejs_bookmark_asset_media_remove'] = array(
			'#type' => 'checkbox', 
			'#title' => t('Remove Image'),
		);
		$form['#attributes'] = array('enctype' => "multipart/form-data");
		$form['#submit'][] = 'timelinejs_form_submit';
	}
}

function timelinejs_form_submit(&$form, &$form_state) {
	if($form_state['values']['timelinejs_bookmark_asset_media_remove']){
		variable_set('timelinejs_bookmark_asset_media',array());

	} else {

		if(!empty($_FILES['files']['name']['timelinejs_bookmark_asset_media2'])){
			$mpath = file_create_path('timelinejs');
			file_check_directory(&$mpath , FILE_CREATE_DIRECTORY);
			$file = array();
			$validators = array('file_validate_extensions' => array('png jpg gif jpeg'));
			$file = file_save_upload('timelinejs_bookmark_asset_media2', $validators, $mpath,FILE_EXISTS_REPLACE);

			if($file){
				variable_set('timelinejs_bookmark_asset_media',$file);
				file_set_status($file, FILE_STATUS_PERMANENT);
			}
		}
	}
}
<?php
/*
*
* @params
*	$path - the module path
*	$css - the css path
*	$attributes - the plugin settings
*		 'width'
*        'height'
*        'startend'
*        'zoom'
*        'lang'
*
*/
?>

<div id="timeline-embed"></div>

<script type="text/javascript">
    var timeline_config = {
		width: 				"<?php print $attributes['width']; ?>",
		height: 			"<?php print $attributes['height']; ?>",
		source: 			"/timeline/get_json",
		start_at_end:		<?php print $attributes['startend']; ?>,
		css: 				"<?php print $css; ?>",
		js: 				"<?php print $path; ?>/js/timeline-min.js",
		start_zoom_adjust:  <?php print $attributes['zoom']; ?>,
		lang:               "<?php print $attributes['lang']; ?>"
	}
</script>
<script type="text/javascript" src="<?php print $path; ?>/js/timeline-embed.js"></script>
